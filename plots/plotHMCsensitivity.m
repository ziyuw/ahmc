%% HMC Sensitivity
% Demonstrate sensitivity of HMC to parameter settings
%
% The figure produced here correspond to figure 1 in the paper.
%
% Shakir and Ziyu, February 2013

%% Run Simulation
% Plotting file: *plotHMCsensitivity.m*

outDir = 'plots';
makePDF = 0; % set to print pdfs and place in pdf dir
rand('state',1);
randn('state',1);

% Specify Gaussian model
target_sigma = [1, 0.99; 0.99, 1];
target_mean = [3; 5];
func = inline('transpose(x-target_mean)*inv(target_sigma)*(x-target_mean)/2');
func = @(x)func(target_mean, target_sigma, x);
gradient  = inline('inv(target_sigma)*(x-target_mean)');
gradient = @(x)gradient(target_mean, target_sigma, x);

% Convergence Behavior
% The good: l = 40 epsilon = 0.05
% The bad:  l = 50 epsilon = 0.05

% Behavior after convergence
% The good: l = 40 epsilon = 0.16
% The bad:  l = 50 epsilon = 0.16
% The ugly: l = 50 epsilon = 0.15

% epsilon = 0.16; l = 40; % good
% epsilon = 0.16; l = 50; % bad
% epsilon = 0.15; l = 50; % ugly

%% Sample for a Gaussian for three settings
% Plot samples and Gaussian contour and autocorrelation
% HMC is known to be highly sensitive to the choice of 
% and L. We demonstrate HMC's sensitivity to these pa-
% rameters by sampling from a bivariate Gaussian with
% correlation coefficient 0.99. We consider three settings
% (e;L) = {(0:16; 40); (0:16; 50); (0:15; 50)} and show the
% behavior of the sampler as well as the autocorrelation
% plot in figure 1. While the first setting exhibits good
% behavior and low auto-correlation, small changes to
% these settings results in poor mixing and high auto-
% correlation, as seen in the other graphs.
epsilonList = [0.16 0.16 0.15];
leapList = [40 50 50];

for qq = 1:length(leapList)
    epsilon = epsilonList(qq);
    l = leapList(qq);
    
    fname = sprintf('plotData_HMCsens_%d.mat',qq);
    if exist(fname,'file')
        load(fname);
    else
        max_iter = 1000;
        q = [-5; -5];
        qs = zeros(max_iter, 2);
        accept_window = zeros(max_iter, 1);
        qs(1, :) = q';
        total_accept = 0;
        
        for i = 1:max_iter
            if mod(i, 100) == 0
                i
            end
            [q, accept, q_prop] = hmc(l, epsilon, q, gradient, func, eye(size(q, 1)));
            qs(i+1, :) = q';
            total_accept = total_accept + accept;
        end
        
        C = acorrtime(qs);
        C = acorr(qs, max_iter - 1);
        
        save(fname,'C','qs', 'epsilon','l', 'qq','total_accept');
    end;
    
    figure;
    % 1. Contours
    subplot(2,2,1);
    xx = qs(:,1);
    yy = qs(:,2);
    scatter(xx, yy, 4);
    p = plot(xx(:), yy(:),'x');
    set(p,'Color','red','LineWidth',1)
    set(gca,'FontSize',12);
    title(sprintf('\\epsilon = %1.2f, L = %d',epsilon,l),'fontsize', 14, 'FontWeight','bold', 'Interpreter', 'Tex');
    hold on;
    x1 = 0:.01:6; x2 = 2:.01:8;
    [X,Y] = meshgrid(x1,x2);
    F = mvnpdf([X(:) Y(:)], target_mean', target_sigma);
    F = reshape(F,length(x2),length(x1));
    contour(x1, x2, F, 5);
    
    if qq == 1
        ylabel('Gaussian, \rho=0.99', 'fontsize', 14, 'FontWeight','bold', 'Interpreter', 'Tex');
    end;
    if qq == 2
        xlim([-5 6]);
    else
        xlim([0 6]);
    end;
    grid on;
    
    % 2. Plot auto-correlation
    h = subplot(2,2,3);
    plot(C(1:100, 1), 'LineWidth',2);
    grid on;
    set(gca,'Ylim',[-0.2, 1])
    xlim([0 100]);
    set(gca,'FontSize',12);
    xlabel('Lag', 'fontsize', 14, 'FontWeight','bold');
    if qq == 1
     ylabel('Auto-correlation',  'fontsize', 14, 'FontWeight','bold');
    end;
    
    if makePDF
        switch qq
            case 1
                outName = 'gaussTrajectory_good';
            case 2
                outName = 'gaussTrajectory_bad';
            case 3
                outName = 'gaussTrajectory_ugly';
        end;    
        printPDF(outName,outDir);
    end;
end;
