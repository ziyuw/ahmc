# Adaptive Hamiltonain Monte Carlo algorithm 
Authors: Ziyu Wang, Shakir Mohamed

This code provides an implementation of adaptive MCMC algorithms for 
Hamiltonian/Hybrid Monte Carlo (AHMC) and Riemannian Manifold Monte Carlo (ARMHMC). 
The algorithms remove the need for manual and expert tuning of the 
hyperparameters governing the performance of these samplers, 
allowing for more efficient sampling and explopration of the posterior 
distribution of many statistical models.

We demonstrate the application of AHMC in sampling from a 
bivariate Gaussian distribution, Bayesian Logistic regression, 
a stochastic volitility model, and a log Gaussian cox point process. 
We provide the code for these models, 
as well as the code to reproduce all the plots in the corresponding paper.

## Installation: 
This package requires no installation. Before running simply call
addpath(genpath(pwd)) in matlab to add all the relavent paths.

## Demos:
To run the demos just cd into the demos folder and run one of the following
scripts:

- demo_ahmc_gaussian.m
- demo_ahmc_BLR.m
- demo_ahmc_LGC.m
- demo_ahmc_stochVol.m

For further reference see [the paper](http://arxiv.org/abs/1302.6182).